import React from "react";

function useRefs(capacity, initValue) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return Array.from({ length: capacity }).map(() => React.useRef(initValue));
}

export default useRefs;
