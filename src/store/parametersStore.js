import create from 'zustand';

const useParametersStore = create((set) => ({
    shuffleCount: 1,
    experimentCount: 1,
    hitsCount: 0,
    totalExperiments: 0,
    simulate: 0,
    isSimulating: false,
    SCChangeBlocked: false,
    doAnimation: true,

    setShuffleCount: (newCount) => set(() => ({
        shuffleCount: newCount,
    })),

    setExperimentCount: (newCount) => set(() => ({
        experimentCount: newCount,
    })),

    increaseTotalExperiments: () => set((state) => ({
        totalExperiments: state.totalExperiments + 1,
    })),

    increaseHits: () => set((state) => ({
        hitsCount: state.hitsCount + 1,
    })),

    startSimulation: () => set((state) => ({
        simulate: state.simulate + 1,
    })),

    setIsSimulating: (newValue) => set(() => ({
        isSimulating: newValue,
    })),

    blockSCChange: () => set((state) => ({
        SCChangeBlocked: true,
    })),

    setDoAnimation: () => set((state) => ({
        doAnimation: !state.doAnimation,
    })),
}));

export default useParametersStore;
