import noop from "lodash.noop";
import Cube from './cube';

class Move{
    faceName;

    targetAngle;

    currentAngle;

    stepAngle;

    complete = noop;

    progress = noop;

    constructor(faceName, inversed, stepAngle) {
        this.faceName = faceName;
        this.stepAngle = stepAngle;
        this.currentAngle = 0;
        this.targetAngle = inversed
            ? Cube.angles.COUNTERCLOCKWISE
            : Cube.angles.CLOCKWISE;
    }

    onComplete(callback) {
        this.complete = callback;
    }

    onProgress(callback) {
        this.progress = callback;
    }

    run() {
        if (this.currentAngle === this.targetAngle) {
          this.complete();
          return;
        }
    
        const targetSign = Math.sign(this.targetAngle)
        this.currentAngle += this.stepAngle * targetSign;
    
        if (Math.abs(this.currentAngle) > Math.abs(this.targetAngle)) {
          this.currentAngle = this.targetAngle;
        }
    
        this.progress(this);
      }
}

export default Move;
