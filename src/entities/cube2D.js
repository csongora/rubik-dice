class Cube2D {
    constructor() {
        this.dotsPerSides = []
        this.rubik2D = [
            [
                [1, 0, 0],
                [0, 0, 0],
                [0, 0, 1]
            ],
            [
                [1, 0, 1],
                [1, 0, 1],
                [1, 0, 1]
            ],
            [
                [1, 0, 1],
                [0, 1, 0],
                [1, 0, 1]
            ],
            [
                [0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]
            ],
            [
                [1, 0, 1],
                [0, 0, 0],
                [1, 0, 1]
            ],
            [
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]
            ]
        ];
    }

    rotate(face, clockwise){
        switch(face) {
            case 'U':
                this.rotateFace(this.rubik2D[4], clockwise);
                this.rotateRow(this.rubik2D[0], this.rubik2D[1], this.rubik2D[2], this.rubik2D[3], [0, 0, 0, 0], clockwise);
                break;
            
            case 'D':
                this.rotateFace(this.rubik2D[5], clockwise);
                this.rotateRow(this.rubik2D[0], this.rubik2D[1], this.rubik2D[2], this.rubik2D[3], [2, 2, 2, 2], !clockwise);
                break;

            case 'F':
                this.rotateFace(this.rubik2D[0], clockwise);

                this.rotateFace(this.rubik2D[1], 1);
                this.rotateFace(this.rubik2D[3], 0);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateRow(this.rubik2D[4], this.rubik2D[1], this.rubik2D[5], this.rubik2D[3], [2, 2, 2, 2], !clockwise);
                this.rotateFace(this.rubik2D[1], 0);
                this.rotateFace(this.rubik2D[3], 1);
                this.rotateFace(this.rubik2D[5], 1);
                this.rotateFace(this.rubik2D[5], 1);
                break;

            case 'B':
                this.rotateFace(this.rubik2D[2], clockwise);

                this.rotateFace(this.rubik2D[1], 1);
                this.rotateFace(this.rubik2D[3], 0);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateRow(this.rubik2D[4], this.rubik2D[1], this.rubik2D[5], this.rubik2D[3], [0, 0, 0, 0], clockwise);
                this.rotateFace(this.rubik2D[1], 0);
                this.rotateFace(this.rubik2D[3], 1);
                this.rotateFace(this.rubik2D[5], 1);
                this.rotateFace(this.rubik2D[5], 1);
                break;

            case 'L':
                this.rotateFace(this.rubik2D[3], clockwise);

                this.rotateFace(this.rubik2D[4], 0);
                this.rotateFace(this.rubik2D[2], 1);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateFace(this.rubik2D[0], 0);
                this.rotateRow(this.rubik2D[4], this.rubik2D[2], this.rubik2D[5], this.rubik2D[0], [0, 0, 0, 0], clockwise);
                this.rotateFace(this.rubik2D[4], 1);
                this.rotateFace(this.rubik2D[2], 0);
                this.rotateFace(this.rubik2D[5], 1);
                this.rotateFace(this.rubik2D[0], 1);
                break;

            case 'R':
                this.rotateFace(this.rubik2D[1], clockwise);

                this.rotateFace(this.rubik2D[4], 0);
                this.rotateFace(this.rubik2D[2], 1);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateFace(this.rubik2D[0], 0);
                this.rotateRow(this.rubik2D[4], this.rubik2D[2], this.rubik2D[5], this.rubik2D[0], [2, 2, 2, 2], !clockwise);
                this.rotateFace(this.rubik2D[4], 1);
                this.rotateFace(this.rubik2D[2], 0);
                this.rotateFace(this.rubik2D[5], 1);
                this.rotateFace(this.rubik2D[0], 1);
                break;

            case 'M':
                this.rotateFace(this.rubik2D[0], 0);
                this.rotateFace(this.rubik2D[4], 0);
                this.rotateFace(this.rubik2D[2], 1);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateRow(this.rubik2D[0], this.rubik2D[4], this.rubik2D[2], this.rubik2D[5], [1, 1, 1, 1], clockwise);
                this.rotateFace(this.rubik2D[0], 1);
                this.rotateFace(this.rubik2D[4], 1);
                this.rotateFace(this.rubik2D[2], 0);
                this.rotateFace(this.rubik2D[5], 1);
                break;

            case 'S':
                this.rotateFace(this.rubik2D[1], 1);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateFace(this.rubik2D[5], 0);
                this.rotateFace(this.rubik2D[3], 0);
                this.rotateRow(this.rubik2D[4], this.rubik2D[1], this.rubik2D[5], this.rubik2D[3], [1, 1, 1, 1], !clockwise);
                this.rotateFace(this.rubik2D[1], 0);
                this.rotateFace(this.rubik2D[5], 1);
                this.rotateFace(this.rubik2D[5], 1);
                this.rotateFace(this.rubik2D[3], 1);
                break;

            case 'E':
                this.rotateRow(this.rubik2D[0], this.rubik2D[1], this.rubik2D[2], this.rubik2D[3], [1, 1, 1, 1], !clockwise);
                break;

            default:
                break;
        }
    }

    rotateRow(matrix1, matrix2, matrix3, matrix4, row, clockwise) {
        if (clockwise) {
            let tmp = matrix1[row[0]];
            matrix1[row[0]] = matrix4[row[3]];
            matrix4[row[3]] = matrix3[row[2]];
            matrix3[row[2]] = matrix2[row[1]];
            matrix2[row[1]] = tmp;
        } else {
            let tmp = matrix1[row[0]];
            matrix1[row[0]] = matrix2[row[1]];
            matrix2[row[1]] = matrix3[row[2]];
            matrix3[row[2]] = matrix4[row[3]];
            matrix4[row[3]] = tmp;
        }
    }

    rotateFace(matrix, clockwise) {
        let n = matrix.length;
        if (clockwise) {
            for (let i = 0; i < n/2; i++) {
                for (let j = i; j < n - i - 1; j++) {
                    let tmp = matrix[i][j];
                    matrix[i][j] = matrix[j][n - i - 1];
                    matrix[j][n - i - 1] = matrix[n - i - 1][n - j - 1];
                    matrix[n - i - 1][n - j - 1] = matrix[n - j - 1][i];
                    matrix[n - j - 1][i] = tmp;
                }
            }
        } else {
            for (let i = 0; i < n/2; i++) {
                for (let j = i; j < n - i - 1; j++) {
                    let tmp = matrix[i][j];
                    matrix[i][j] = matrix[n - j - 1][i];
                    matrix[n - j - 1][i] = matrix[n - i - 1][n - j - 1];
                    matrix[n - i - 1][n - j - 1] = matrix[j][n - i - 1];
                    matrix[j][n - i - 1] = tmp;
                }
            }
        }
    }

    countDots() {
        this.dotsPerSides = []
        let counter;
        for(let i = 0; i < 6; i++) {
            counter = 0;
            for (let j = 0; j < 3; j++) {
                for (let k = 0; k < 3; k++) {
                    if (this.rubik2D[i][j][k] === 1)
                        counter += 1;
                }
            }
            this.dotsPerSides.push(counter);
        }
    }

    isOk() {
        this.countDots();
        let counter = 0;
        for (let i = 0; i < 6; i++) {
            if (this.dotsPerSides[i] === 4) {
                counter += 1;
            } 
        }

        return counter === 3;
    }

    printMatrix() {
        for(let i = 0; i < 6; i++)
            console.table(this.rubik2D[i]);
    }

    reset() {
        this.rubik2D = [
            [
                [1, 0, 0],
                [0, 0, 0],
                [0, 0, 1]
            ],
            [
                [1, 0, 1],
                [1, 0, 1],
                [1, 0, 1]
            ],
            [
                [1, 0, 1],
                [0, 1, 0],
                [1, 0, 1]
            ],
            [
                [0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]
            ],
            [
                [1, 0, 1],
                [0, 0, 0],
                [1, 0, 1]
            ],
            [
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]
            ]
        ];
    }
}

export default Cube2D;
