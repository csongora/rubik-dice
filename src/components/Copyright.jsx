import { Typography, Link } from '@mui/material';
import React from 'react';

function Copyright() {
  return (
    <Typography
      variant="body2"
      sx={{
        fontWeight: 'bold',
      }}
    >
      {'Copyright © '}
      <Link
        color="inherit"
        href="https://www.facebook.com/profile.php?id=100009661801179"
        underline="hover"
      >
        Salamon Csongor
      </Link>
      {' '}
      {new Date().getFullYear()}
      .
    </Typography>
  );
}

export default Copyright;
