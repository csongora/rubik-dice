import React from "react";
import { Canvas } from '@react-three/fiber';
import MyRubikDice from "./RubikDice";
import { OrbitControls, Instances, Instance, Sky } from '@react-three/drei';

const Grid = ({ number = 23, lineWidth = 0.026, height = 0.5 }) => (
  // Renders a grid and crosses as instances
  <Instances position={[0, -9, 0]}>
    <planeGeometry args={[lineWidth, height]} />
    <meshBasicMaterial color="#999" />
    {Array.from({ length: number }, (_, y) =>
      Array.from({ length: number }, (_, x) => (
        <group key={x + ':' + y} position={[x * 2 - Math.floor(number / 2) * 2, -0.01, y * 2 - Math.floor(number / 2) * 2]}>
          <Instance rotation={[-Math.PI / 2, 0, 0]} />
          <Instance rotation={[-Math.PI / 2, 0, Math.PI / 2]} />
        </group>
      ))
    )}
    <gridHelper args={[100, 100, '#bbb', '#bbb']} position={[0, -0.01, 0]} />
  </Instances>
)

function MyCanvas() {
  return(
    <Canvas shadows gl={{ preserveDrawingBuffer: true }}
      camera={{position: [0, 0, 10]}}
    >
      <ambientLight intensity={0.2} />
      <spotLight intensity={0.4} angle={0.2} penumbra={1} position={[-30, 30, -30]} castShadow shadow-mapSize={[512, 512]} />
      <directionalLight intensity={0.5} position={[-10, 10, -10]} color="white" />
      <MyRubikDice />
      <Grid />
      <OrbitControls />
      <Sky />
    </Canvas>
  );
}

export default MyCanvas;
