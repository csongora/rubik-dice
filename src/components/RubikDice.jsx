import React, { useRef, useEffect, useState } from "react";
import { useFrame } from '@react-three/fiber';
import * as THREE from 'three';

import useRefs from "../hooks/useRefs";
import Cube from "../entities/cube";
import Cube2D from "../entities/cube2D";
import Move from "../entities/move";
import facesMeta from "../meta";
import rotateAroundWorldAxis from "../utils/helpers";
import getRandomMoves from "../utils/getRandomMoves";

import useParametersStore from "../store/parametersStore";

const boxSize = 0.95;

const dotInitialPositions = [
  [0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0],
  [0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 1, 0, 1], [0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 1, 0],
  [0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0],
  [1, 0, 1, 0, 0, 1], [0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 1, 0],
  [1, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0],
  [1, 0, 0, 1, 0, 1], [0, 0, 0, 0, 0, 0], [1, 0, 0, 1, 0, 0],
];

function MyCube({x, y, z, hasDot, size = boxSize, groupRef}) {
  const [textures] = useState(() =>
    ['noDot', 'dot'].map(name =>
      new THREE.TextureLoader().load(`/pics/${name}.png`)
    )
  )

  return(
    <group ref={groupRef} >
      <mesh dispose position={[x + size/2, y, z]} rotation={[0, Math.PI/2, 0]} castShadow receiveShadow>
        <planeGeometry attach="geometry" args={[size, size]} />
        <meshStandardMaterial map={hasDot[0] === 1 ? textures[1] : textures[0]} metalness={0.3} roughness={0.5} />
      </mesh>
      
      <mesh dispose position={[x - size/2, y, z]} rotation={[0, -Math.PI/2, 0]} castShadow receiveShadow>
        <planeGeometry attach="geometry" args={[size, size]} />
        <meshStandardMaterial map={hasDot[1] === 1 ? textures[1] : textures[0]} metalness={0.3} roughness={0.5} />
      </mesh>
      
      <mesh dispose position={[x, y - size/2, z]} rotation={[Math.PI/2, 0, 0]} castShadow receiveShadow>
        <planeGeometry attach="geometry" args={[size, size]} />
        <meshStandardMaterial map={hasDot[2] === 1 ? textures[1] : textures[0]} metalness={0.3} roughness={0.5} />
      </mesh>
      
      <mesh dispose position={[x, y + size/2, z]} rotation={[-Math.PI/2, 0, 0]} castShadow receiveShadow>
        <planeGeometry attach="geometry" args={[size, size]} />
        <meshStandardMaterial map={hasDot[3] === 1 ? textures[1] : textures[0]} metalness={0.3} roughness={0.5} />
      </mesh>
      
      <mesh dispose position={[x, y, z + size/2]} rotation={[0, 0, 0]} castShadow receiveShadow>
        <planeGeometry attach="geometry" args={[size, size]} />
        <meshStandardMaterial map={hasDot[4] === 1 ? textures[1] : textures[0]} metalness={0.3} roughness={0.5} />
      </mesh>
      
      <mesh dispose position={[x, y, z - size/2]} rotation={[0, Math.PI, 0]} castShadow receiveShadow>
        <planeGeometry attach="geometry" args={[size, size]} />
        <meshStandardMaterial map={hasDot[5] === 1 ? textures[1] : textures[0]} metalness={0.3} roughness={0.5} />
      </mesh>
    </group>
  );
}

function MyRubikDice() {
  const defaultStepAngle = 3;

  const boxRefs = useRefs(27);
  const moveEntityRef = useRef();
  const cubeEntityRef = React.useRef(new Cube());
  const cube2D = new Cube2D();

  const simulate = useParametersStore((state) => state.simulate);
  const shuffleCount = useParametersStore((state) => state.shuffleCount);
  const experimentCount = useParametersStore((state) => state.experimentCount);
  const doAnimation = useParametersStore((state) => state.doAnimation);
  const setIsSimulating = useParametersStore((state) => state.setIsSimulating);
  const increaseTotalExperiments = useParametersStore((state) => state.increaseTotalExperiments);
  const increaseHits = useParametersStore((state) => state.increaseHits);

  const rotateMeshs = (faceName, degrees) => {
    const facePieces = cubeEntityRef.current.faces[faceName];

    for (let i = 0; i < 9; i += 1) {
      const piece = facePieces[i];
      if (!boxRefs[piece.key].current) {
        return;
      }
    }

    for (let i = 0; i < 9; i += 1) {
      const piece = facePieces[i]
      rotateAroundWorldAxis(
        boxRefs[piece.key].current,
        facesMeta[faceName].axis,
        degrees * (Math.PI / 180.0)
      )
    }
  }

  const rotateFace = async (
    faceName,
    inversed,
    stepAngle = defaultStepAngle
  ) => {
    if (moveEntityRef.current) {
      return Promise.resolve();
    }

    return new Promise(resolve => {
      moveEntityRef.current = new Move(faceName, inversed, stepAngle);

      moveEntityRef.current.onComplete(() => {
        cubeEntityRef.current.rotate(faceName, inversed);
        moveEntityRef.current = undefined;
        resolve();
      })

      moveEntityRef.current.onProgress(move => {
        const targetSign = Math.sign(move.targetAngle);
        const rotationFactor = facesMeta[faceName].inverse
          ? -targetSign
          : targetSign;
        rotateMeshs(faceName, stepAngle * rotationFactor);
      })
    })
  }

  const scrambleFaces = async () => {
    if (moveEntityRef.current) {
      return;
    }
    cube2D.reset();

    const [moves, reversedMoves] = getRandomMoves(shuffleCount);

    for (let i = 0; i < moves.length; i++) {
      const { faceName, inversed } = moves[i];
      if (doAnimation) await rotateFace(faceName, inversed, defaultStepAngle * 3);
      cube2D.rotate(faceName, inversed);
    }

    if (cube2D.isOk()) {
      increaseHits();
      console.log('-------' + cube2D.dotsPerSides + '-------')
    } else {
      console.log(cube2D.dotsPerSides.toString());
    }

    if (doAnimation)
      for (let i = 0; i < reversedMoves.length; i++) {
        const { faceName, inversed } = reversedMoves[i];
        await rotateFace(faceName, inversed, 90);
      }
  }

  useFrame(() => {
    if (moveEntityRef.current) {
      moveEntityRef.current.run();
    }
  })

  useEffect(() => {
    return () => {
      if (moveEntityRef.current) {
        moveEntityRef.current.complete();
      }
    }
  }, [])

  useEffect(() => {
    if(simulate !== 0){
      for ( let i = 0; i < experimentCount; i++ ) {
        setTimeout(() => {
          scrambleFaces();
          increaseTotalExperiments();
          setIsSimulating(true);
        }, doAnimation ? 300 * shuffleCount * i : 0);
      }

      setTimeout(() => {
        setIsSimulating(false);
      }, doAnimation ? 300 * shuffleCount * experimentCount : 0);
    }
  }, [simulate]);

  return(
    <React.Suspense fallback={null}>
      <MyCube x={-1} y={-1} z={-1} hasDot={dotInitialPositions[0]} groupRef={boxRefs[24]} />
      <MyCube x={-1} y={-1} z={0} hasDot={dotInitialPositions[1]} groupRef={boxRefs[15]} />
      <MyCube x={-1} y={-1} z={1} hasDot={dotInitialPositions[2]} groupRef={boxRefs[6]} />
      <MyCube x={-1} y={0} z={-1} hasDot={dotInitialPositions[3]} groupRef={boxRefs[21]} />
      <MyCube x={-1} y={0} z={0} hasDot={dotInitialPositions[4]} groupRef={boxRefs[12]} />
      <MyCube x={-1} y={0} z={1} hasDot={dotInitialPositions[5]} groupRef={boxRefs[3]} />
      <MyCube x={-1} y={1} z={-1} hasDot={dotInitialPositions[6]} groupRef={boxRefs[18]} />
      <MyCube x={-1} y={1} z={0} hasDot={dotInitialPositions[7]} groupRef={boxRefs[9]} />
      <MyCube x={-1} y={1} z={1} hasDot={dotInitialPositions[8]} groupRef={boxRefs[0]} />
      <MyCube x={0} y={-1} z={-1} hasDot={dotInitialPositions[9]} groupRef={boxRefs[25]} />
      <MyCube x={0} y={-1} z={0} hasDot={dotInitialPositions[10]} groupRef={boxRefs[16]} />
      <MyCube x={0} y={-1} z={1} hasDot={dotInitialPositions[11]} groupRef={boxRefs[7]} />
      <MyCube x={0} y={0} z={-1} hasDot={dotInitialPositions[12]} groupRef={boxRefs[22]} />
      <MyCube x={0} y={0} z={0} hasDot={dotInitialPositions[13]} groupRef={boxRefs[13]} />
      <MyCube x={0} y={0} z={1} hasDot={dotInitialPositions[14]} groupRef={boxRefs[4]} />
      <MyCube x={0} y={1} z={-1} hasDot={dotInitialPositions[15]} groupRef={boxRefs[19]} />
      <MyCube x={0} y={1} z={0} hasDot={dotInitialPositions[16]} groupRef={boxRefs[10]} />
      <MyCube x={0} y={1} z={1} hasDot={dotInitialPositions[17]} groupRef={boxRefs[1]} />
      <MyCube x={1} y={-1} z={-1} hasDot={dotInitialPositions[18]} groupRef={boxRefs[26]} />
      <MyCube x={1} y={-1} z={0} hasDot={dotInitialPositions[19]} groupRef={boxRefs[17]} />
      <MyCube x={1} y={-1} z={1} hasDot={dotInitialPositions[20]} groupRef={boxRefs[8]} />
      <MyCube x={1} y={0} z={-1} hasDot={dotInitialPositions[21]} groupRef={boxRefs[23]} />
      <MyCube x={1} y={0} z={0} hasDot={dotInitialPositions[22]} groupRef={boxRefs[14]} />
      <MyCube x={1} y={0} z={1} hasDot={dotInitialPositions[23]} groupRef={boxRefs[5]} />
      <MyCube x={1} y={1} z={-1} hasDot={dotInitialPositions[24]} groupRef={boxRefs[20]} />
      <MyCube x={1} y={1} z={0} hasDot={dotInitialPositions[25]} groupRef={boxRefs[11]} />
      <MyCube x={1} y={1} z={1} hasDot={dotInitialPositions[26]} groupRef={boxRefs[2]} />
    </React.Suspense>
  );
}

export default MyRubikDice;
