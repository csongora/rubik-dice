import * as React from 'react';
import { AppBar, Box, Toolbar, Typography } from '@mui/material';

export default function ButtonAppBar() {
  return (
    <Box>
      <AppBar position="static">
        <Toolbar>
          <Typography variant='h5'>
            Rubik's Dice Problem
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}