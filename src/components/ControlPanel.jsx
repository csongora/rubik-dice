import React from "react";
import { Button, Card, Slider, Typography, Divider, Switch } from "@mui/material";
import useParametersStore from "../store/parametersStore";

function ControlPanel() {
    const experimentCount = useParametersStore((state) => state.experimentCount);
    const shuffleCount = useParametersStore((state) => state.shuffleCount);
    const SCChangeBlocked = useParametersStore((state) => state.SCChangeBlocked);
    const isSimulating = useParametersStore((state) => state.isSimulating);
    const doAnimation = useParametersStore((state) => state.doAnimation);
    const setExperimentCount = useParametersStore((state) => state.setExperimentCount);
    const setShuffleCount = useParametersStore((state) => state.setShuffleCount);
    const startSimulation = useParametersStore((state) => state.startSimulation);
    const blockSCChange = useParametersStore((state) => state.blockSCChange);
    const setDoAnimation = useParametersStore((state) => state.setDoAnimation);

    function valuetext(value) {
        return `${value}°C`;
    }

    return(
        <Card
            sx={{
                ml: 2,
                mr: 1,
                height: '100%',
                zIndex: 2,
                bgcolor: '#f5feff',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center'
            }}
            elevation={4}
        >
            <Typography
                    variant="h5"
                    sx={{ alignSelf: 'center', mt: 3 }}
                >
                    Turn on Simulation
            </Typography>
            <Switch
                checked={doAnimation}
                onChange={setDoAnimation}
                sx={{ alignSelf: 'center', mt: 3, mb: 3 }}
                disabled={isSimulating}
            />
            <Divider />
            <Typography
                variant="h5"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                Experiment Count
            </Typography>
            <Typography
                variant="h3"
                color="purple"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                {experimentCount}
            </Typography>
            <Slider
                disabled={isSimulating}
                aria-label="experimentCount"
                defaultValue={experimentCount}
                value={experimentCount}
                getAriaValueText={valuetext}
                valueLabelDisplay="auto"
                onChange={(e) => setExperimentCount(e.target.value)}
                step={doAnimation ? 1 : 100}
                marks
                min={1}
                max={doAnimation ? 10 : 10000}
                sx={{
                    color: '#eb5515',
                    mb: 3,
                    width: '80%',
                    mt: 3,
                    alignSelf: 'center'
                }}
            />
            <Divider />
            <Typography
                variant="h5"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                Shuffle Count
            </Typography>
            <Typography
                variant="h3"
                color="purple"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                {shuffleCount}
            </Typography>
            <Slider
                disabled={SCChangeBlocked}
                aria-label="shuffleCount"
                defaultValue={shuffleCount}
                value={shuffleCount}
                getAriaValueText={valuetext}
                valueLabelDisplay="auto"
                onChange={(e) => setShuffleCount(e.target.value)}
                step={1}
                marks
                min={1}
                max={20}
                sx={{
                    color: '#eb5515',
                    mb: 3,
                    width: '80%',
                    mt: 3,
                    alignSelf: 'center'
                }}
            />
            <Divider />
            <Button
                disabled={isSimulating}
                onClick={() => {startSimulation(); blockSCChange();}}
                variant="contained"
                sx={{
                    width: '90%',
                    mt: 5,
                    ml: 2
                }}
            >
                Shuffle
            </Button>
        </Card>
    );
}

export default ControlPanel;
