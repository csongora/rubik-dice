import React from "react";
import { Card, Typography, Divider } from "@mui/material";
import useParametersStore from "../store/parametersStore";

function ResultsPanel() {
    const totalExperiments = useParametersStore((state) => state.totalExperiments);
    const hitsCount = useParametersStore((state) => state.hitsCount);

    return(
        <Card
            sx={{
                bgcolor: '#f5feff',
                mr: 2,
                ml: 1, 
                height: '100%',
                zIndex: 2,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center'
            }}
            elevation={4}
        >
            <Divider />
            <Typography
                variant="h4"
                sx={{ alignSelf: 'center', mt: 6 }}
            >
                Experiments
            </Typography>
            <Typography
                variant="h3"
                color="green"
                sx={{ alignSelf: 'center', mb: 6 }}
            >
                {totalExperiments}
            </Typography>
            <Divider />
            <Typography
                variant="h4"
                sx={{ alignSelf: 'center', mt: 6 }}
            >
                OK
            </Typography>
            <Typography
                variant="h3"
                color="orange"
                sx={{ alignSelf: 'center', mb: 6 }}
            >
                {hitsCount}
            </Typography>
            <Divider />
            <Typography
                variant="h4"
                sx={{ alignSelf: 'center', mt: 6 }}
            >
                Probability
            </Typography>
            <Typography
                variant="h3"
                color="red"
                sx={{ alignSelf: 'center', mb: 6 }}
            >
                {totalExperiments !== 0 ? (hitsCount / totalExperiments).toFixed(4) : "?"}
            </Typography>
            <Divider />
        </Card>
    );
}

export default ResultsPanel;
