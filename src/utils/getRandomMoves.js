import shuffle from 'lodash.shuffle';

export default function getRandomMoves(quantity) {
    const faceNames = shuffle(['U', 'D', 'F', 'B', 'L', 'R', 'M', 'S', 'E'])
    const moves = []
    const reverseMoves = []

    let reversed = true;
    for (let i = 0; i < quantity; i++) {
        const x = i % faceNames.length

        reversed = Math.random() > 0.5

        moves[i] = {
            faceName: faceNames[x],
            inversed: reversed,
        }

        reverseMoves[quantity - i - 1] = {
            faceName: faceNames[x],
            inversed: !reversed,
        }
    }

    return [moves, reverseMoves];
}