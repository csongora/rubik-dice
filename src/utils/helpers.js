import * as THREE from 'three';

function rotateAroundWorldAxis(
    mesh,
    axisVector,
    radians
) {
    const quaternion = new THREE.Quaternion();
    quaternion.setFromAxisAngle(axisVector, radians);

    mesh.quaternion.multiplyQuaternions(quaternion, mesh.quaternion);
    // mesh.position.sub(axisVector);
    // console.log("🚀 ~ file: helpers.js:13 ~ axisVector", axisVector)
    mesh.position.applyQuaternion(quaternion);
    // console.log("🚀 ~ file: helpers.js:14 ~ quaternion", quaternion)
    // mesh.position.add(axisVector);
}

export default rotateAroundWorldAxis;
