import './App.css';

import React from 'react';
import ButtonAppBar from './components/ButtonAppBar';
import AppFooter from './components/AppFooter';
import { Box, Grid } from '@mui/material';
import MyCanvas from './components/MyCanvas';
import ControlPanel from './components/ControlPanel';
import ResultsPanel from './components/ResultsPanel';

function App() {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      }}
    >
      <ButtonAppBar />
        <Grid container spacing={1} sx={{ height: '87vh' }}>
          <Grid item xs={2} sx={{ zIndex: 1, mt: 3 }}>
            <ControlPanel />
          </Grid>
          <Grid
            item
            xs={8}
            id="canvas-container"
            sx={{
              zIndex: 0,
              display: 'flex',
              flexDirection: 'column',
              alignContent: 'center',
              justifyContent: 'center'
            }}
          >
            <MyCanvas />
          </Grid>
          <Grid item xs={2}  sx={{ zIndex: 1, mt: 3 }}>
            <ResultsPanel />
          </Grid>
        </Grid>
      <AppFooter />
    </Box>
  );
}

export default App;
